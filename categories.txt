{"es":"museos", "en":"museum"}	
{"es":"monumentos", "en":"monuments"}	
{"es":"arte", "en":"art"}	
{"es":"gastronomia", "en":"gastronomy"}	
{"es":"bares", "en":"bars"}	
{"es":"compras", "en":"shopping"}	
{"es":"alojamiento", "en":"lodging"}	
{"es":"noche", "en":"night life"}	
{"es":"naturaleza", "en":"nature"}	
{"es":"niños", "en":"kids"}	
{"es":"historia", "en":"history"}	
{"es":"en familia", "en":"in family"}	
{"es":"aventura", "en":"adventure"}	
{"es":"curiosidades", "en":"curiosities"}	
{"es":"lujo", "en":"luxe"}	
{"es":"deportes", "en":"sports"}	