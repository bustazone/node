'use strict';

// Load required packages
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy
var ClientPasswordStrategy  = require('passport-oauth2-client-password').Strategy;
var errorHandler = require('./errors.server.controller');
var User = require('../models/user.server.model');
var Client = require('../models/client.server.model');
var Token = require('../models/token.server.model');

passport.use(new BasicStrategy({
    passReqToCallback: true
  },
  function(req, username, password, callback) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return callback(err); }

      // No user found with that username
      if (!user) { return callback(null, false); }

      // Make sure the password is correct
      user.verifyPassword(password, function(err, isMatch) {
        if (err) { return callback(err); }

        // Password did not match
        if (!isMatch) { return callback(null, false); }

        req.login(user, function(err) {
          if (err) {
            return callback(null, false);
          }
          // Success
          return callback(null, user);
        });
        
      });
    });
  }
));

passport.use(new LocalStrategy(
  function(username, password, callback) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return callback(err); }
      if (!user) {
        return callback(null, false, { message: 'Incorrect username.' });
      }
      user.verifyPassword(password, function(err, isMatch) {
        if (err) { return callback(err); }

        // Password did not match
        if (!isMatch) { return callback(null, false, { message: 'Incorrect password.' }); }

        return callback(null, user);
        
      });
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

passport.use('client-basic', new BasicStrategy(
  function(username, password, callback) {
    console.log("---------ClientBasic");
    console.log(username);
    Client.findOne({ id: username }, function (err, client) {
      if (err) { return callback(err); }

      console.log(client);

      // No client found with that id or bad password
      if (!client || client.secret !== password) { return callback(null, false); }

      // Success
      return callback(null, client);
    });
  }
));

passport.use(new BearerStrategy(
  function(accessToken, callback) {
    console.log("---------------"+accessToken);
    Token.findOne({accessToken: accessToken }, function (err, token) {
      if (err) { return callback(err); }
      console.log(err);
      console.log(token);
      // No token found
      console.log(callback);
      if (!token) { return callback(null, false, {message: "Incorrect token"}); }

      console.log("expirationDate");
      console.log(token.expirationDate);

      if (new Date().getTime() > token.expirationDate.getTime()) {
        token.remove(function(err) {
          console.log("now");
          if (err) return callback(err);
          console.log("now");
          callback(null, false, {message: "Token is expired"});
        });
      } else {
        console.log(token);
        console.log(token.userId);

        token.expirationDate = token.getNewExpirationDate();
        token.save(function(err) {
          if (err) console.log(err);
          console.log("----New Token--->");
          console.log(token);
        });

        User.findById(token.userId, function (err, user) {
          console.log(user);
          if (err) { return callback(err); }

          // No user found
          if (!user) { return callback(null, false, {message: "Can't find user: "+token.userId}); }

          // Simple example with no scope
          return callback(null, user, { scope: '*' });
        });
      }
    });
  }
));

passport.use(new ClientPasswordStrategy(
  function(clientId, clientSecret, done) {
    console.log("---------ClientPasswordStrategy");
    console.log(clientId);
    console.log(clientSecret);
    Client.findOne({ id: clientId }, function(err, client) {
        if (err) { return done(err); }
        if (!client) { return done(null, false); }
        if (client.secret != clientSecret) { return done(null, false); }

        return done(null, client);
    });
  }
));

// passport.use("clientPassword", new ClientPasswordStrategy(
//     function (clientId, clientSecret, done) {
//         db.collection('clients').findOne({clientId: clientId}, function (err, client) {
//             if (err) return done(err)
//             if (!client) return done(null, false)
//             //if (!client.trustedClient) return done(null, false)
 
//             if (client.clientSecret == clientSecret) return done(null, client)
//             else return done(null, false)
//         });
//     }
// ));

//exports.isAuthenticated = passport.authenticate('basic', { 
  //The option of session being set to false tells passport to not store session variables between calls to our API. This forces the user to submit the username and password on each call.
//  session : false 
//});

//exports.isUserAuthenticated = passport.authenticate(['basic', 'bearer'], { session : false });
exports.isUserAuthenticated = passport.authenticate(['local']);
exports.isUserLogged = function(req, res, next) {
  console.log(req);
  if (req.user) next()
  else res.redirect('/login')
};
exports.isClientAuthenticated = passport.authenticate(['oauth2-client-password','client-basic'], { session: false });
//exports.isBearerAuthenticated = passport.authenticate('bearer', { session: false });
exports.isBearerAuthenticated = function(req, res, next) { 
  passport.authenticate('bearer', { session: false }, function(err, user, info) {
    if (err) { return res.send(info.error_description); }
    if (!user) { return res.status(401).send(info); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return next();
    });
  })(req, res, next);
};
exports.isBearerAdminAuthenticated = function(req, res, next) { 
  passport.authenticate('bearer', { session: false }, function(err, user, info) {
    if (err) { return res.send(info.error_description); }
    if (!user) { return res.status(401).send(info); }
    if (user.role != 'ADMIN') { return res.status(401).send(info); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return next();
    });
  })(req, res, next);
};
exports.isManagementAuthenticated = function (req, res, next) {
  
  if (req.headers.key === "clientsManagementKey")
  {
    console.log("kdkdkd");
    next();
  }
  else
  {
    console.log("bdmdbmd");
    return res.status(401).send({
      message: "Incorrect key for client management"
    });
  }
};


exports.signup = function(req, res) {

  var username = req.body.username;
  var password = req.body.password;
  var redirect_uri = req.body.redirect_uri;
  var client_id = req.body.client_id;
  var response_type = req.body.response_type;

  var user = new User({
    username: username,
    password: password
  });

  user.save(function(err) {
    if (err) 
    {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    else 
    {
      //return res.json({ message: 'New User Created', user: user });
      return res.redirect('/oauth2/authorize?username='+username+'&password='+password+'&response_type='+response_type+'&client_id='+client_id+'&redirect_uri='+redirect_uri);
    }
  });
};

exports.signout = function (req, res) {
  var userId = req.user._id 

  req.logout();

  console.log("logging out");
  
  Token.find({ userId: userId }, function (err, tokens) {
    if (err) { 
      return res.status(400).json({message: "logged out succesfully but Token doesn't found"});
    }

    for (var i = 0; i<tokens.length; i++)
    {
      tokens[i].remove(function(err) {
        // if(err) { 
        //   errorHandler.getErrorMessage(err)  
        // }
      });
    }

    console.log("tokens deleted");

    res.status(200).json({message: "logged out succesfully"});

  });
}