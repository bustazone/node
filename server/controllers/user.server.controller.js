// Load required packages
var User = require('../models/user.server.model');
var errorHandler = require('./errors.server.controller')

// Create endpoint /api/users for POST
exports.createUser = function(req, res) {
  var user = new User({
    username: req.body.username,
    password: req.body.password
  });

  user.save(function(err) {
    if (err) 
    {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    else 
    {
      return res.json({ message: 'New User Created', user: user });
    }
  });
};

// Create endpoint /api/users for GET
exports.listUsers = function(req, res) {

  var page = req.body.page;
  var letter = req.body.alphabeticFilter;

  var query = User.find().sort('-username');
  
  if (letter)
  {
    query.where('name', new RegExp('\b'+letter, "i"))
  }

  if (page)
  {
    query.skip(page*50)
  }

  q.limit(50).exec(function(err, users) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(users);
    }
  });
};


exports.deleteUser = function (req, res) {
  User.findById(req.query.id, function (err, user) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }
    if(!user) { 
      return res.send(404); 
    }
    user.remove(function(err) {
      if(err) { 
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });  
      }
      return res.status(200).json({ message: 'User Deleted'});
    });
  });
};


exports.detailUser = function (req, res) {
  User.findById(req.query.id, function (err, user) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }
    if(!user) { 
      return res.send(404); 
    }
    
    return res.status(200).json(user);
  });
};


exports.updateUser = function (req, res) {
  var jsonBody = req.body;
  var user = new User(jsonBody);
  Category.findById(jsonBody._id, function(err, p) {
    if (!p)
      //return next(new Error('Could not load Document'));
      return res.send('Could not load Document');
    else {
      // do your updates here
      if (jsonBody.username)
      {
        p.username = user.username;
      }

      p.save(function(err) {
        if (err)
          return res.status(201).json({"name" : user.username, "error" : errorHandler.getErrorMessage(err)});
        else
          return res.status(201).json(p);
      });
    }
  });
};


exports.detailCurrentUser = function (req, res) {
  User.findById(req.user._id, function (err, user) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }
    if(!user) { 
      return res.send(404); 
    }
    
    return res.status(200).json(user);
  });
};


exports.updateCurrentUser = function (req, res) {
  var jsonBody = req.body;
  var user = new User(jsonBody);
  User.findById(req.user._id, function(err, user) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }

    if (!p)
      //return next(new Error('Could not load Document'));
      return res.send('Could not load Document');
    else {
      // do your updates here
      if (user.username)
      {
        p.username = user.username;
      }

      p.save(function(err) {
        if (err)
          return res.status(201).json({"name" : user.username, "error" : errorHandler.getErrorMessage(err)});
        else
          return res.status(201).json(p);
      });
    }
  });
};


exports.changePassword = function (req, res) {
  var jsonBody = req.body;
  var user = new User(jsonBody);
  User.findById(req.user._id, function(err, p) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }

    if (!p)
      //return next(new Error('Could not load Document'));
      return res.send('Could not load Document');
    else {

      p.verifyPassword(user.oldPassword, function(err, isMatch) {
        

        if(err) { 
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          }); 
        }

        // Password did not match
        if (!isMatch) { 
          return res.status(400).send({
            message: 'Incorrect old password'
          });
        }

        if (user.newPassword === user.repeatNewPassword)
        {
          p.password = user.newPassword;

          p.save(function (err) {
            if (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            } 
            
            return res.status(200).send({
              message: 'Password changed'
            });
          });
        }
      })
    }
  });
};