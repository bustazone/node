'use strict';

var oauth2orize = require('oauth2orize');
var utils = require('../utils/oauth2.utils');
var errorHandler = require('./errors.server.controller');
var User = require('../models/user.server.model');
var Client = require('../models/client.server.model');
var Token = require('../models/token.server.model');
var Code = require('../models/code.server.model');

// Create OAuth 2.0 server
var server = oauth2orize.createServer();

// Register serialialization function
server.serializeClient(function(client, callback) {
  return callback(null, client._id);
});

// Register deserialization function
server.deserializeClient(function(id, callback) {
  Client.findOne({ _id: id }, function (err, client) {
    if (err) { return callback(err); }
    return callback(null, client);
  });
});

// Register authorization code grant type
server.grant(oauth2orize.grant.code(function(client, redirectUri, user, ares, callback) {
  // Create a new authorization code
  console.log("--------->create code");
  var code = new Code({
    value: utils.uid(16),
    clientId: client._id,
    redirectUri: redirectUri,
    userId: user._id
  });

  // Save the auth code and check for errors
  code.save(function(err) {
    if (err) { return callback(err); }

    callback(null, code.value);
  });
}));

server.grant(oauth2orize.grant.token(function (client, user, ares, done) {
  console.log("--------->create token");
  console.log(client.__id);
  console.log(client);
  var token = new Token({
    accessToken: utils.uid(256),
    refreshToken: utils.uid(256),
    clientId: client._id,
    userId: user._id
  });

  // Save the access token and check for errors
  token.save(function (err) {
    console.log("--------->token created");
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      return done(err);
    }
    //return done(null, token);
    //return done(null, null, token);

    return done(null, token.accessToken);
    //return done(null, token.accessToken, { expirationDate: token.expirationDate.toString() });
  });

}))

// Exchange authorization codes for access tokens
server.exchange(oauth2orize.exchange.code(function(client, code, redirectUri, callback) {

  console.log(code);
  Code.findOne({ value: code }, function (err, authCode) {
    console.log(err);
    if (err) { return callback(err); }
    console.log(authCode);
    if (authCode === undefined) { return callback(null, false); }
    console.log(client._id.toString());
    console.log(authCode.clientId);
    if (client._id.toString() !== authCode.clientId) { return callback(null, false); }
    console.log(redirectUri);
    if (redirectUri !== authCode.redirectUri) { return callback(null, false); }



    // Delete auth code now that it has been used
    authCode.remove(function (err) {
      if(err) { return callback(err); }

      // Create a new access token
      var token = new Token({
        accessToken: utils.uid(256),
        refreshToken: utils.uid(256),
        clientId: authCode.clientId,
        userId: authCode.userId
      });

      console.log(callback);
      console.log(token);

      // Save the access token and check for errors
      token.save(function (err) {
        console.log("ffffffffffffffffffffffffffffffffff");
        console.log(err);
        console.log("ffffffffffffffffffffffffffffffffff");
        if (err) { return callback(err); }

        console.log("ffffffffffffffffffffffffffffffffff");
        console.log(token);
        callback(null, token);
      });
    });
  });
}));

server.exchange(oauth2orize.exchange.password(function (client, username, password, scope, done) {
  console.log("--------->oauth2 password");
  console.log(client);
    User.findOne({username: username}, function (err, user) {
        if (err) return done(err);
        if (!user) return done(null, false);
        user.verifyPassword(password, function(err, isMatch) {
        
          if (err) { return callback(err); }

          // Password did not match
          if (!isMatch) { return callback(null, false); }
 
          var token = new Token({
            accessToken: utils.uid(256),
            refreshToken: utils.uid(256),
            clientId: client._id,
            userId: user._id
          });

          // Save the access token and check for errors
          token.save(function (err) {
            if (err) return done(err);
            done(null, token);
          });
        })
    })
}))

// Exchange refresh_token for access tokens
server.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {

  Token.findOne({ refreshToken: refreshToken }, function (err, token) {
    if (err) { return done(err); }
    if (!token) { return done(null, false); }
    if (client._id.toString() !== token.clientId) { return done(null, false); }

    token.accessToken = utils.uid(256);

    token.save(function (err) {
      if (err) { return done(err); }

      done(null, token);
    });

  });
}));

// User authorization endpoint
exports.authorization = [
  server.authorization(function(clientId, redirectUri, callback) {

    console.log("--------->authorization starts");
    console.log(clientId);
    Client.findOne({ id: clientId }, function (err, client) {
      console.log(callback);
      if (err) { return callback(err); }

      return callback(null, client, redirectUri);
    });
  },
  //Esta función "inmediate" decide si es necesario solicitar una decisión o no
  function (client, user, done) {
    console.log("--------->Inmediate");
    console.log(client);
    console.log(user);
    console.log(done);

    if (client.auto_approve)
    {
      return done(null,true);
    }
    else
    {
      return done(null,false);
    }

  }),
  function(req, res){
    console.log("--------->Show decision dialog");
    //.sendfile('../../public/aouth2_Dialog.html');  
    res.render('decisionDialog', { transactionID: req.oauth2.transactionID, user: req.user, client: req.oauth2.client });
  }
]

// User decision endpoint
exports.decision = [
  server.decision()
]

// Application client token exchange endpoint
exports.token = [
  server.token(),
  server.errorHandler()
]