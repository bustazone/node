// Load required packages
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Client = require('../models/client.server.model');

// Create endpoint /api/client for POST
exports.createClient = function(req, res) {
  // Create a new instance of the Client model
  var client = new Client();

  // Set the client properties that came from the POST data
  client.name = req.body.name;
  client.id = req.body.id;
  client.secret = req.body.secret;
  client.auto_approve = req.body.auto_approve;
  client.redirectURI = req.body.redirectURI;

  // Save the client and check for errors
  client.save(function(err) {
    if (err) return res.send(err);

    return res.status(201).json({ message: 'Client added: ', data: client });
  });
};

exports.updateClient = function (req, res) {
  console.log(req.user);
  console.log(req.body);

  var jsonBody = req.body;
  var newClient = new Client(jsonBody);
  Client.findById(jsonBody._id, function(err, oldClient) {
    if (!oldClient)
      //return next(new Error('Could not load Document'));
      return res.send('Could not find client');
    else {
      // do your updates here
      if (jsonBody.id)
      {
        oldClient.id = newClient.id;
      }

      if (jsonBody.name)
      {
        oldClient.name = newClient.name;
      }

      if (jsonBody.secret)
      {
        oldClient.secret = newClient.secret;
      }

      if (jsonBody.auto_approve)
      {
        oldClient.auto_approve = newClient.auto_approve;
      }

      if (jsonBody.redirectURI)
      {
        oldClient.redirectURI = newClient.redirectURI;
      }

      oldClient.save(function(err) {
        if (err)
          return res.status(201).json({"name" : oldClient.name, "error" : errorHandler.getErrorMessage(err)});
        else
          return res.status(201).json(oldClient);
      });
    }
  });
};

exports.deleteClient = function (req, res) {
  Client.findById(req.body.id, function (err, client) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }
    if(!client) { 
      return res.send(404); 
    }
    client.remove(function(err) {
      if(err) { 
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });  
      }
      return res.sendStatus(204);
    });
  });
};

// Create endpoint /api/clients for GET
exports.listClients = function(req, res) {
  // Use the Client model to find all clients
  Client.find(function(err, clients) {
    if (err) res.send(err);

    res.json(clients);
  });
};

exports.detailClient = function (req, res) {
  Client.findById(req.user._id, function (err, client) {
    if(err) { 
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      }); 
    }
    if(!client) { 
      return res.send(404); 
    }
    return res.status(201).json(client);
  });
};