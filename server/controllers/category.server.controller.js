'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Category = mongoose.model('Category')

/**
 * Create a 
 */
exports.create = function(req, res) {

	var out = {errors: [], success: []};
	var count = 0;
	var category;

	var jsonBody = req.body;
	var strBody = JSON.stringify(jsonBody);

	console.log(jsonBody);
	console.log(strBody);

	function errFnc(err) {
		if (err) {
			out.errors.push({"name" : category.name, "error" : errorHandler.getErrorMessage(err)});
		} else {
			out.success.push(category);
		}
		count++;
		if (count === jsonBody.length || strBody.startsWith("{"))
		{
			return res.status(201).json(out);	
		}
	}

	if (strBody.startsWith('{'))
	{
		category = new Category(jsonBody);
	  category.save(errFnc);
	}
	else
	{
		for (var i=0; i<jsonBody.length; i++){
	    category = new Category(jsonBody[i]);
	    category.save(errFnc);
		}
	}
};

/**
 * Show the current 
 */
exports.read = function(req, res) {
	Category.findById(req.params.categoryId).exec(function(err, category) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (!category) {
				return res.status(404).send({
					message: 'Category not found'
				});
			}
			res.json(category);
    }
  });
};

/**
 * Update a 
 */
exports.update = function (req, res) {
	var jsonBody = req.body;
	var category = new Category(jsonBody);
	Category.findById(jsonBody._id, function(err, p) {
	  if (!p)
	    //return next(new Error('Could not load Document'));
	  	return res.send('Could not load Document');
	  else {
	    // do your updates here
	    if (jsonBody.name)
	    {
	    	p.name = category.name;
	    }

	    if (jsonBody.description)
	    {
	    	p.description = category.description;
	    }

	    p.save(function(err) {
	      if (err)
	      	return res.status(201).json({"name" : category.name, "error" : errorHandler.getErrorMessage(err)});
	      else
	        return res.status(201).json(p);
	    });
	  }
	});
};

/**
 * Delete an 
 */
exports.delete = function (req, res) {
	Category.findById(req.query.id, function (err, category) {
    if(err) { 
    	return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
    	}); 
  	}
    if(!category) { 
    	return res.send(404); 
    }
    category.remove(function(err) {
      if(err) { 
      	return res.status(400).send({
        	message: errorHandler.getErrorMessage(err)
    		});  
      }
      return res.send(204);
    });
  });
};

/**
 * List of 
 */
exports.list = function(req, res) {
  //console.log("--dddddddddd--");
  //var sess=req.session;
  //console.log(sess);
  //console.log(sess.email);
  //console.log(req.user);
  //console.log(req);

   // equivalent to $_SESSION['email'] in PHP.
//sess.username; 
  Category.find().limit(50).exec(function(err, categories) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(categories);
    }
  });
};

exports.listFilter = function(req, res) {
	var jsonBody = req.body;
  var q = Category.find();
  if (jsonBody.nameFilter)
  {
		q.where('name', new RegExp('^'+jsonBody.nameFilter, "i"));
	}
  q.exec(function(err, categories) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(categories);
    }
  });
};
