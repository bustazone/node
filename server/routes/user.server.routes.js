'use strict';

var authController = require('../controllers/auth.server.controller');

module.exports = function(app) {
  // Routing logic   
  //app.route('/categories')
  //	.get(function (request, response) {
  //  	response.json([{ name: 'Beverages' },{ name: 'Condiments' }]);
  //});
  var users = require('../controllers/user.server.controller');

  app.route('/users/list')
    .get(authController.isManagementAuthenticated, users.listUsers);

  app.route('/users/create')
    .post(authController.isManagementAuthenticated, users.createUser);

  app.route('/users/deleteUser')
    .get(authController.isManagementAuthenticated, users.deleteUser); 

  app.route('/users/detailUser')
    .get(authController.isManagementAuthenticated, users.detailUser);

  app.route('/users/updateUser')
    .post(authController.isManagementAuthenticated, users.updateUser);

  app.route('/users/detailCurrentUser')
    .get(authController.isBearerAuthenticated, users.detailCurrentUser);  

  app.route('/users/updateCurrentUser')
    .get(authController.isBearerAuthenticated, users.updateCurrentUser);    
}