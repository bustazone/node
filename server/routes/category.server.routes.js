'use strict';

var authController = require('../controllers/auth.server.controller');

module.exports = function(app) {
  // Routing logic   
  //app.route('/categories')
  //	.get(function (request, response) {
  //  	response.json([{ name: 'Beverages' },{ name: 'Condiments' }]);
  //});
  var categories = require('../controllers/category.server.controller');

  app.route('/categories/list')
    //.get(authController.isAuthenticated, categories.list)
    .get(authController.isBearerAuthenticated, categories.list)
    .post(authController.isBearerAuthenticated, categories.listFilter);

  app.route('/categories/create')
    .post(authController.isManagementAuthenticated, categories.create);

  app.route('/categories/update')
    .post(authController.isManagementAuthenticated, categories.update);

  //app.route('/categories/delete/:categoryId')
  app.route('/categories/delete')
		.get(authController.isManagementAuthenticated, categories.delete);
};
