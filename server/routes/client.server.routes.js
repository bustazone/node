'use strict';

var clientController = require('../controllers/client.server.controller');
var authController = require('../controllers/auth.server.controller');

module.exports = function(app) {

	// Create endpoint handlers for /clients
	app.route('/oauth2/clients/create')
		.post(authController.isManagementAuthenticated, clientController.createClient);

	app.route('/oauth2/clients/update')
		.post(authController.isClientAuthenticated, clientController.updateClient);

	app.route('/oauth2/clients/delete')
		.post(authController.isManagementAuthenticated, clientController.deleteClient);

	app.route('/oauth2/clients/list')
		.post(authController.isManagementAuthenticated, clientController.listClients);

	app.route('/oauth2/clients/detail')
		.get(authController.isClientAuthenticated, clientController.detailClient);
		
}