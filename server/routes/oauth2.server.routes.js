var authController = require('../controllers/auth.server.controller');
var auth2Controller = require('../controllers/auth2.server.controller');

module.exports = function(app) {

	app.route('/oauth2/islogged')
	  .get(authController.isBearerAuthenticated, function(req,res){res.send(200);});

	//inicio para las estrategias "authorization_code" (code) y "implicit" (token)
	app.route('/oauth2/authorize')
	  .get(authController.isUserAuthenticated, auth2Controller.authorization);

	//endpoint para la respuesta a la decision de permiso de acceso a scopes
	app.route('/oauth2/decision')
		.get(authController.isUserLogged, auth2Controller.decision);

	//inicio para las estrategias refresh token (grant_type=refresh_token), 
	//password (grant_type=password) y para el intercambio de token por code en 
	//la estrategia "authorization_code" (grant_type=authorization_code)
	//(tiene que ser post obligatoriamente)
	app.route('/oauth2/token')
		.post(authController.isClientAuthenticated, auth2Controller.token);

	//endpoint para registro de usuario con login para las estrategias 
	//"authorization_code" (code) y "implicit" (token)
	app.route('/oauth2/signup')
	  .post(authController.signup);

	//endpoint para logout
	app.route('/oauth2/logout')
	  .get(authController.isBearerAuthenticated, authController.signout);

}