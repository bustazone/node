// Load required packages
var mongoose = require('mongoose');

// Define our client schema
var ClientSchema = new mongoose.Schema({
  name: { type: String, required: true },
  id: { type: String, unique: true, required: true },
  secret: { type: String, required: true },
  redirectURI: { type: String, required: true },
  auto_approve: { type: Boolean, default: false }
});

// Export the Mongoose model
module.exports = mongoose.model('Client', ClientSchema);