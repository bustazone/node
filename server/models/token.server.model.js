// Load required packages
var mongoose = require('mongoose');
var config = require('../config.server');

// Define our token schema
// var TokenSchema   = new mongoose.Schema({
//   value: { type: String, required: true },
//   userId: { type: String, required: true },
//   clientId: { type: String, required: true }
// });
var TokenSchema = new mongoose.Schema({
  accessToken: { type: String, required: true },
  refreshToken: { type: String, required: true },
  expirationDate: { type: Date },
  userId: { type: String, required: true },
  clientId: { type: String, required: true }
});

// Execute before each user.save() call
TokenSchema.pre('save', function(callback) {

  this.expirationDate = this.getNewExpirationDate();	

  console.log("---------->");
  console.log(this.expirationDate);

  callback();
});

TokenSchema.methods.getNewExpirationDate = function() {
  return new Date(new Date().getTime() + config.outh2_config.validInactiveTime);
};

// Export the Mongoose model
module.exports = mongoose.model('Token', TokenSchema);