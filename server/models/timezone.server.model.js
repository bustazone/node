// Load required packages
var mongoose = require('mongoose');

// Define our token schema
var TimeZoneSchema   = new mongoose.Schema({
	ISO_Country: { type: String, required: true },
 	name: { type: String, required: true },
 	GMT_offset_jan2016: { type: Number, required: true },
 	GMT_offset_jul2016: { type: Number, required: true },
 	raw_offset: { type: Number, required: true }
});

TimeZoneSchema.index({name: 1}, {unique: true});

// Export the Mongoose model
module.exports = mongoose.model('TimeZone', TimeZoneSchema);