// Load required packages
var mongoose = require('mongoose');

// Define our token schema
var CountrySchema   = new mongoose.Schema({
  ISO: { type: String },
	ISO3: { type: String },
 	ISO_num: { type: Number },
 	fips: { type: String },
 	name: { type: String },
 	capital: { type: String },
 	area: { type: Number },
 	population: { type: Number },
 	continent: { type: String },
 	TLD: { type: String },
 	CurrencyCode: { type: String },
 	CurrencyName: { type: String },
 	PhonePrefix: { type: String },
 	PostalCodeFormat: { type: String },
 	PostalCodeRegex: { type: String },
 	Languages: { type: [String] },
 	neighbours: { type: [String] },
});

// Export the Mongoose model
module.exports = mongoose.model('Country', CountrySchema);