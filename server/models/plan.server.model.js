// Load required packages
var mongoose = require('mongoose');

// Define our token schema
var PlanSchema = new mongoose.Schema({
	Title: { 
		type: String, 
		required: true 
	},
	Description: { 
		type: String },
	URL: { 
		type: String, 
		required: true , 
		validate: {
	        validator: function(v) {
	            return /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(v);
	        },
	        message: '{VALUE} is not a valid phone number!'
	    } 
	},
	MainImage: { 
		type: String, 
		required: true, 
		validate: {
	        validator: function(v) {
	            return /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(v);
	        },
	        message: '{VALUE} is not a valid phone number!'
	    } 
	},
	MainImageTN: { 
		type: String, 
		required: true, 
		validate: {
	        validator: function(v) {
	            return /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(v);
	        },
	        message: '{VALUE} is not a valid phone number!'
	    }
	},
	Location: { 
		type: String, 
		required: true 
	},
	Categories: { 
		type: [String], 
		required: true 
	},
	Tags: { 
		type: String 
	},
	Duración: { 
		type: Number 
	},
	PuntualGPSLatitude: { 
		type: Number 
	},
	PuntualGPSLongitude: { 
		type: Number 
	},
	User: { 
		type: String, 
		required: true 
	}

});

// Export the Mongoose model
module.exports = mongoose.model('TimeZone', TimeZoneSchema);