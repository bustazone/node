'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Category Schema
 */
var CategorySchema = new Schema({
    // the property name
  description: {
    type: String,
    default: '',
    // types have specific functions e.g. trim, lowercase, uppercase (http://mongoosejs.com/docs/api.html#schema-string-js)
    trim: true
  },
  name: {
    type: String,
    default: '',
    trim: true,     
    unique : true,
    // make this a required field
    required: 'name cannot be blank'
  }
});

module.exports = mongoose.model('Category', CategorySchema);
