// Load required packages
var mongoose = require('mongoose');

// Define our token schema
var LocationSchema   = new mongoose.Schema({
	name: { type: String, required: true },
	country: { type: String, required: true },
	loc_level1: { type: String },
	loc_level2: { type: String },
	loc_level3: { type: String },
	loc_level4: { type: String },
	latitude: { type: Number },
	longitude: { type: Number },
	timezone: { type: String },
	type: { type: String,
      enum: ["region","location"],
      required: true }
});

// Export the Mongoose model
module.exports = mongoose.model('Location', LocationSchema);