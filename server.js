// Get the packages we need
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var passport = require('passport');
var authController = require('./server/controllers/auth.server.controller');

var session = require('express-session');

var ejs = require('ejs');


//models
var category = require('./server/models/category.server.model');
var user = require('./server/models/user.server.model');

// Create our Express application
var app = express();

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Use express session support since OAuth2orize requires it
app.use(session({
  secret: 'Super Secret Session Key',
  saveUninitialized: true,
  resave: true,
  cookie:{maxAge:60000}
}));

// Use the passport package in our application
app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'ejs');

// Use environment defined port or 3000
var port = process.env.PORT || 3000;

// Start the server
app.listen(port);
console.log('Insert beer on port ' + port);

// Connect to the MongoDB
mongoose.connect('mongodb://localhost:27017/beerlocker');

//routes
//Esta función se ejecuta siempre que el path empice por '/'. 
//Se ha de ponerantes que cualquier route
app.use('/', function (req, res, next) {
  console.log("--dddd--");
  // if (req.path !== '/login')
  // {
  // 	return res.redirect('/login');
  // }
  next();
});
app.use('/', express.static('angular_example'));
require("./server/routes/category.server.routes")(app);
require("./server/routes/user.server.routes")(app);
require("./server/routes/client.server.routes")(app);
require("./server/routes/oauth2.server.routes")(app);