

    angular
        .module('app', ['ui.router','login','ngCookies','myConfig'])
        .config(config)
        .run(run)
        .controller('apiAppCtrl', ['$http','$scope','$rootScope', '$location','$cookies', 'oauth2_constants', 'sharedProperties', controladorPrincipal])
        .controller('homeCtrl', ['$http','$scope', '$location','$cookies', 'oauth2_constants', 'sharedProperties', controladorHome]);
        //.controller('apiAppCtrl', ['$http', controladorPrincipal]);

    function config($stateProvider, $urlRouterProvider) {
        // default route
        $urlRouterProvider.otherwise("/");
 
        $stateProvider
            .state('home', {
                url: '/',
                views: {
                'main_ui_view': {
                        templateUrl: '/home.html',     
                        controller: 'homeCtrl',
                        controllerAs: 'vm'
                    }
                },
                data: { activeTab: 'home' }
                //templateUrl: './home.html',,
            })
            .state('account', {
                url: '/account',
                views: {
                'main_ui_view': {
                        templateUrl: './account.html',
                        controller: 'apiAppCtrl',
                        controllerAs: 'vm'
                    }
                },
                data: { activeTab: 'account' }
            })
            .state('account.example', {
                url: '/ex',
                views: {
                'profile_inner_ui_view': {
                        templateUrl: './example.html'
                    }
                },
                data: { activeTab: 'account', ActiveSubsectionTab:'ex' }
            });
    }
 
    function run($http, $cookies, $rootScope, sharedProperties) {

        var loginRequiredStates = ['account'];

        console.log("run run run");

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            $rootScope.activeTab = toState.data.activeTab;
            $rootScope.ActiveSubsectionTab = toState.data.ActiveSubsectionTab;
            $rootScope.Logged = sharedProperties.ifLogged(function (logged) {
                console.log("lndlknw--------loged  "+logged);
                $rootScope.Logged = logged;
                var loginBut = angular.element( document.querySelector( '#loginBut' ) );
                var logoutBut = angular.element( document.querySelector( '#logoutBut' ) );
                if (logged)
                {
                    loginBut.addClass('displayHide');
                    logoutBut.removeClass('displayHide');
                }
                else
                {
                    loginBut.removeClass('displayHide');
                    logoutBut.addClass('displayHide');
                }
            });
            console.log(toState.data.activeTab);
            console.log($rootScope.activeTab);
            console.log(toState);
            $rootScope.showVeil = false;
            if (!sharedProperties.getAccessTokenAuthHeader() && (loginRequiredStates.indexOf(toState.name) > -1))
            {
                $rootScope.showVeil = true
                //alert("se muestra la pantalla de redirección a login");
            }
        })

        $rootScope.logout = function () {
            var myEl = angular.element( document.querySelector( '#overVeil' ) );
            myEl.addClass('displayShow');

            $http.get("/oauth2/logout", {
                headers: {'Authorization': sharedProperties.getAccessTokenAuthHeader()}
            })
            .then(function(res){
                // acciones a realizar cuando se recibe respuesta con éxito
                console.log(res);
            }, function(res){
                // acciones a realizar cuando se recibe una respuesta de error
                console.log(res);
            });
            sharedProperties.clearAccessToken();
            alert("logedout");

            window.location = "/";
        }

        // if ($cookies.getObject('oauth2_data'))
        // {
        //     console.log($cookies.getObject('oauth2_data'));
        //     console.log(JSON.parse($cookies.getObject('oauth2_data')));
        //     console.log(JSON.parse($cookies.getObject('oauth2_data')).access_token);
        //     //console.log(JSON.parse($cookies.getObject('oauth2_data')).expirationDate);
        //     console.log(JSON.parse($cookies.getObject('oauth2_data')).token_type);

        //     // add default auth header with cookies data
        //     var auth_header = 'Bearer ' + JSON.parse($cookies.getObject('oauth2_data')).access_token;
        //     console.log(auth_header);
        //     $http.defaults.headers.common['Authorization'] = auth_header;
        // }
    }

    function controladorPrincipal($http,$scope,$location,$cookies,$rootScope,oauth2_constants,sharedProperties){
        var vm=this;
        
        vm.buscaEnRegion = function(){
            console.log('------->'+$rootScope.showVeil);
            $rootScope.showVeil = true;
            console.log('------->'+$rootScope.showVeil);

            // var myEl = angular.element( document.querySelector( '#overVeil' ) );
            // myEl.addClass('displayShow');

            // $http.get(vm.url).success(function(respuesta){
            //     //console.log("res:", respuesta);
            //     vm.paises = respuesta;
            // });
            
            console.log('----> '+sharedProperties.getAccessTokenAuthHeader());

            $http.get("/users/detailCurrentUser", {
                headers: {'Authorization': sharedProperties.getAccessTokenAuthHeader()}
            })
            .then(function(res){
                // acciones a realizar cuando se recibe respuesta con éxito
                console.log(res);
            }, function(res){
                // acciones a realizar cuando se recibe una respuesta de error
                console.log("sdfsfsdfsdf");
                console.log(res);
                if (res.status === 401)
                {
                    sharedProperties.clearAccessToken();
                    alert("Logueate!!!!");
                }
            });

            //myEl.removeClass('displayShow');
        }
    }


    function controladorHome($http,$scope,$location,$cookies,oauth2_constants,sharedProperties)
    {
        var vm=this;

        vm.items = [
            {image:"http://www.html5canvastutorials.com/demos/assets/darth-vader.jpg"},
            {image:"http://www.blogodisea.com/wp-content/uploads/2010/03/chuches-chucherias-chicles-infancia-pasado-sugus.jpg"},
            {image:"http://sweet-design.es/wp-content/uploads/2013/10/GRG02_FRESITAS-DE-CHUCHE-ROJAS-EN-MADRID.jpg"},
            {image:"http://imworld.aufeminin.com/dossiers/D20140109/chuches2-1-134320_L.jpg"},
            {image:"http://www.badamelos.com/blog-golosinas-online/wp-content/uploads/2013/02/portada-chuches-online-bdm-3.jpg"},
            {image:"http://www.proveedores.com/articulos/wp-content/uploads/2014/10/proveedores-tiendas-chuches.jpg"},
            {image:"http://www.elcorreo.com/vizcaya/ocio/noticias/201401/31/Media/gominolas-caseras--647x321.jpg"},
            {image:"http://www.elrincondelaspequenassonrisas.es/wp-content/uploads/2014/05/shutterstock_143216152.jpg"},
            {image:"http://pequelia.republica.com/files/2008/11/chuches-gominolas.jpg"},
            {image:"http://www.disgo.es/wp-content/uploads/2015/10/chuches-al-por-mayor.jpg"},
            {image:"http://www.gosarion.com/images/stories/virtuemart/product/chuches7.jpg"},
            {image:"http://sweet-design.es/wp-content/uploads/2014/03/GRG13_CHUCHES-ROSAS.jpg"},
            {image:"http://elmejorbocadillo.com/404-thickbox_default/maceta-de-chuches.jpg"},
            {image:"http://1.bp.blogspot.com/-EfCw1BXy3QE/VpjN7JHAMwI/AAAAAAAAEj0/j733_JD4n90/s900/PASTEL%2BCHUCHES%2BCORAZ%25C3%2593N.jpg"},
            {image:"http://sweet-design.es/wp-content/uploads/2014/05/GRG81_CHUCHES-DE-FRESA-CON-PICA-PICA-ONLINE.jpg"}

        ];
        for (var i = 0; i < vm.items.length; i++)
        {
            var row = Math.floor(i/5);
            var column = i%5;
            var odd = (column%2)!=0;
            console.log(row + ' ' + column + ' ' + odd);

            var top = row*218;
            if (odd) top+=109;
            var left = column * 189;
            vm.items[i].id = "c"+i;
            vm.items[i].idover = "c"+i+"over";
            vm.items[i].idfore = "c"+i+"fore";
            vm.items[i].styles = "position: absolute;top: "+top+"px;left: "+left+"px;";
            //vm.items.push({id:"c"+i, idover:"c"+i+"over", styles: "position: absolute;top: "+top+"px;left: "+left+"px;"});
        }

        // vm.items = [
        //     {id:"c1", idover:"c1over", styles: "position: absolute;top: 0px;left: 0px;"},
        //     {id:"c2", idover:"c2over", styles: "position: absolute;top: 109px;left: 189px;"},
        //     {id:"c3", idover:"c3over", styles: "position: absolute;top: 0px;left: 378px;"},
        //     {id:"c4", idover:"c4over", styles: "position: absolute;top: 109px;left: 567px;"},
        //     {id:"c5", idover:"c5over", styles: "position: absolute;top: 0px;left: 756px;"},
        //     {id:"c6", idover:"c6over", styles: "position: absolute;top: 218px;left: 0px;"},
        //     {id:"c7", idover:"c7over", styles: "position: absolute;top: 327px;left: 189px;"}
        // ];
    // }

    // function fillCanvas($http,$scope,$location,$cookies,oauth2_constants,sharedProperties)
    // {
        console.log("ddlkdlkd");
        for (var i = 0; i < vm.items.length; i++) {
            var imageObj = new Image();
            imageObj.myCustomData = {index: i};
            imageObj.src = vm.items[i].image;

            imageObj.onload = function()
            {
                var c = document.getElementById(vm.items[this.myCustomData.index].idfore);
                var ctx = c.getContext("2d");
                //ctx.shadowBlur=20;
                //ctx.shadowColor='rgb(80,80,80)';
                ctx.save();
                ctx.translate(5,4);
                ctx.scale(0.96, 0.96);
                ctx.beginPath();

                ctx.moveTo(83, 0);
                  ctx.lineTo(169, 0);
                  ctx.quadraticCurveTo(189,0,199,17);
                  ctx.lineTo(242, 92);
                  ctx.quadraticCurveTo(252,109,242,126);
                  ctx.lineTo(199, 201);
                  ctx.quadraticCurveTo(189,218,169,218);
                  ctx.lineTo(83, 218);
                  ctx.quadraticCurveTo(63,218,53,201);
                  ctx.lineTo(10, 126);
                  ctx.quadraticCurveTo(0,109,10,92);
                  ctx.lineTo(53, 17);
                  ctx.quadraticCurveTo(63,0,83,0);
                
                ctx.strokeStyle = 'rgb(80,80,80)';
                ctx.lineWidth = 12;
                ctx.stroke();




            
                var c = document.getElementById(vm.items[this.myCustomData.index].id);
                var ctx = c.getContext("2d");
                ctx.shadowBlur=20;
                ctx.shadowColor='rgb(80,80,80)';
                ctx.beginPath();
                //ctx.moveTo(0, 0);
                //ctx.lineTo(300, 150);

                ctx.moveTo(83, 0);
                  ctx.lineTo(169, 0);
                  ctx.quadraticCurveTo(189,0,199,17);
                  ctx.lineTo(242, 92);
                  ctx.quadraticCurveTo(252,109,242,126);
                  ctx.lineTo(199, 201);
                  ctx.quadraticCurveTo(189,218,169,218);
                  ctx.lineTo(83, 218);
                  ctx.quadraticCurveTo(63,218,53,201);
                  ctx.lineTo(10, 126);
                  ctx.quadraticCurveTo(0,109,10,92);
                  ctx.lineTo(53, 17);
                  ctx.quadraticCurveTo(63,0,83,0);


                ctx.clip();
                if (this.width >= this.height)
                    ctx.drawImage(this, (252 - (252 * this.width / this.height))/2, 0, 252 * this.width / this.height, 218);
                else
                    ctx.drawImage(this, 0, (218 - (218 * this.height / this.width))/2, 252, 218 * this.height / this.width);



                ctx.beginPath();
                ctx.moveTo(10, 126);
                ctx.lineTo(53, 201);
                ctx.lineTo(83, 218);
                ctx.lineTo(169, 218);
                ctx.lineTo(199, 201);
                ctx.lineTo(242, 126);
                ctx.quadraticCurveTo(126,180,10,126);
                ctx.fillStyle = "rgba(80,80,80,0.7)";
                ctx.fill();
                
                // ctx.strokeStyle = 'rgb(80,80,80)';
                // ctx.lineWidth = 5;
                // ctx.stroke();

                ctx.translate(5,4);
                ctx.scale(0.96, 0.96);
                ctx.beginPath();

                ctx.moveTo(83, 0);
                ctx.lineTo(169, 0);
                ctx.quadraticCurveTo(189,0,199,17);
                ctx.lineTo(242, 92);
                ctx.quadraticCurveTo(252,109,242,126);
                ctx.lineTo(199, 201);
                ctx.quadraticCurveTo(189,218,169,218);
                ctx.lineTo(83, 218);
                ctx.quadraticCurveTo(63,218,53,201);
                ctx.lineTo(10, 126);
                ctx.quadraticCurveTo(0,109,10,92);
                ctx.lineTo(53, 17);
                ctx.quadraticCurveTo(63,0,83,0);
                
                ctx.strokeStyle = 'rgb(80,80,80)';
                ctx.lineWidth = 12;
                ctx.stroke();





                c = document.getElementById(vm.items[this.myCustomData.index].idover);
                ctx = c.getContext("2d");
                // ctx.beginPath();
                // ctx.moveTo(83, 0);
                // ctx.lineTo(169, 0);
                // ctx.quadraticCurveTo(189,0,199,17);
                // ctx.lineTo(242, 92);
                // ctx.quadraticCurveTo(252,109,242,126);
                // ctx.lineTo(199, 201);
                // ctx.quadraticCurveTo(189,218,169,218);
                // ctx.lineTo(83, 218);
                // ctx.quadraticCurveTo(63,218,53,201);
                // ctx.lineTo(10, 126);
                // ctx.quadraticCurveTo(0,109,10,92);
                // ctx.lineTo(53, 17);
                // ctx.quadraticCurveTo(63,0,83,0);
                // ctx.fillStyle = "rgba(20,20,20,0.7)";
                // ctx.fill();

                ctx.beginPath();
                ctx.moveTo(20, 109);
                ctx.lineTo(66, 109);
                ctx.lineTo(96, 57);
                ctx.lineTo(73, 17);
                ctx.lineTo(20, 109);
                ctx.fillStyle = "rgb(220,220,220)";
                ctx.fill();
                ctx.strokeStyle = 'rgb(80,80,80)';
                ctx.lineWidth = 5;
                ctx.stroke();

                ctx.beginPath();
                ctx.moveTo(73, 17);
                ctx.lineTo(179, 17);
                ctx.lineTo(156, 57);
                ctx.lineTo(96, 57);
                ctx.lineTo(73, 17);
                ctx.fillStyle = "rgb(220,220,220)";
                ctx.fill();
                ctx.strokeStyle = 'rgb(80,80,80)';
                ctx.lineWidth = 5;
                ctx.stroke();

                ctx.beginPath();
                ctx.moveTo(179, 17);
                ctx.lineTo(232, 109);
                ctx.lineTo(186, 109);
                ctx.lineTo(156, 57);
                ctx.lineTo(179, 17);
                ctx.fillStyle = "rgb(220,220,220)";
                ctx.fill();
                ctx.strokeStyle = 'rgb(80,80,80)';
                ctx.lineWidth = 5;
                ctx.stroke();

                ctx.translate(5,4);
                ctx.scale(0.96, 0.96);
                ctx.beginPath();

                ctx.moveTo(83, 0);
                ctx.lineTo(169, 0);
                ctx.quadraticCurveTo(189,0,199,17);
                ctx.lineTo(242, 92);
                ctx.quadraticCurveTo(252,109,242,126);
                ctx.lineTo(199, 201);
                ctx.quadraticCurveTo(189,218,169,218);
                ctx.lineTo(83, 218);
                ctx.quadraticCurveTo(63,218,53,201);
                ctx.lineTo(10, 126);
                ctx.quadraticCurveTo(0,109,10,92);
                ctx.lineTo(53, 17);
                ctx.quadraticCurveTo(63,0,83,0);
                
                ctx.strokeStyle = 'rgb(80,80,80)';
                ctx.lineWidth = 12;
                ctx.stroke();
            }
        }
    }

  // var canvas = document.getElementById('c');
  // var context = canvas.getContext('2d');

  // context.beginPath();
  // context.moveTo(0,0);
  // context.lineTo(100,100);
  // context.stroke();

  // context.beginPath();
  // context.moveTo(25, 0);
  // context.lineTo(75, 0);
  // context.lineTo(100, 50);
  // context.lineTo(75, 100);
  // context.lineTo(25, 100);
  // context.lineTo(0, 50);
  // context.lineTo(25, 0);
  // context.strokeStyle = 'blue';
  // context.stroke();

/*
  var imageObj = new Image();

  imageObj.src = 'http://www.html5canvastutorials.com/demos/assets/darth-vader.jpg';

  imageObj.onload = function()
  {

      context.save();
      context.beginPath();
      context.moveTo(25, 0);
      context.lineTo(75, 0);
      context.lineTo(100, 50);
      context.lineTo(75, 100);
      context.lineTo(25, 100);
      context.lineTo(0, 50);
      context.lineTo(25, 0);
      context.clip();
      context.drawImage(imageObj, 0, 0, 100, 100 * imageObj.height / imageObj.width);

      context.strokeStyle = 'blue';
      context.stroke();
  };
*/
//}

