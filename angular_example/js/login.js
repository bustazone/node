angular
    .module('login', ['ngCookies','myConfig'])
    .controller('loginCtrl', ['$scope', 'oauth2_constants', controladorLogin])
    .controller('registerCtrl', ['$scope', 'oauth2_constants', controladorRegister])
    .controller('loggedCtrl', ['$scope', '$location','$cookies', 'sharedProperties', controladorLogged])
    .service('sharedProperties', function ($http, $cookies, oauth2_constants) {
        return {
            getAccessTokenAuthHeader: function () {
                var data = $cookies.getObject(oauth2_constants.cookies_name);
                console.log(data);
                if (data && JSON.parse(data).access_token && JSON.parse(data).access_token != '')
                {
                    //return JSON.parse(data).access_token;
                    return JSON.parse(data).token_type+' '+JSON.parse(data).access_token;
                }
                return false;
            },
            getTokenData: function() {
                var data = $cookies.getObject(oauth2_constants.cookies_name);
                if (data)
                {
                    return JSON.parse(data);
                }
                return false;
            },
            setTokenData: function(value) {
                console.log('setTokenData ----->');
                console.log(oauth2_constants.cookies_name);
                console.log(value);
                console.log(new Date(new Date().getTime() + oauth2_constants.cookies_expiration_time));
                $cookies.remove(oauth2_constants.cookies_name);
                $cookies.putObject(oauth2_constants.cookies_name, JSON.stringify(value), {path: '/', expires: new Date(new Date().getTime() + oauth2_constants.cookies_expiration_time)});

                var data = $cookies.getObject(oauth2_constants.cookies_name);
                console.log(data);
            },
            clearAccessToken: function () {
                $cookies.remove(oauth2_constants.cookies_name);
            },
            ifLogged: function(fun) {
                var data = $cookies.getObject(oauth2_constants.cookies_name);
                if (data && JSON.parse(data).access_token && JSON.parse(data).access_token != '')
                {
                    console.log("1")
                    $http.get("/oauth2/islogged", {
                        headers: {'Authorization': JSON.parse(data).token_type+' '+JSON.parse(data).access_token}
                    })
                    .then(function(res){
                        // acciones a realizar cuando se recibe respuesta con éxito
                        console.log("2")
                        if (fun)
                        {
                            fun(true);
                        }
                        //return true;
                        
                    }, function(res){
                        // acciones a realizar cuando se recibe una respuesta de error
                        console.log("3")
                        if (fun)
                        {
                            $cookies.remove(oauth2_constants.cookies_name);
                            fun(false);
                        }
                        //return false;
                    });
                    return true;
                }
                else
                {
                    //return false;
                    fun(false);
                }
            }
        };
    });

function controladorLogin($scope, oauth2_constants){
    var vm = this;

    vm.client_id = oauth2_constants.client_id;
    
    vm.redirect_uri = oauth2_constants.redirect_uri;;
    
    vm.response_type = "token";
    
}


function controladorRegister($scope, oauth2_constants){
    var vm = this;

    vm.client_id = oauth2_constants.client_id;
    
    vm.redirect_uri = oauth2_constants.redirect_uri;;
    
    vm.response_type = "token";

}
    

function controladorLogged($scope, $location, $cookies, sharedProperties){
    var vm = this;

    if ($location.absUrl().indexOf('access_token')>0)
    {
        vm.access_token = $location.absUrl().split('access_token=')[1];
        if (vm.access_token.indexOf('&')>0)
        {
            vm.access_token = vm.access_token.split('&')[0];
        }
    }

    // if ($location.absUrl().indexOf('expirationDate')>0)
    // {
    //     vm.expirationDate = $location.absUrl().split('expirationDate=')[1];
    //     console.log(vm.expirationDate);
    //     if (vm.expirationDate.indexOf('&')>0)
    //     {
    //         vm.expirationDate = vm.expirationDate.split('&')[0];
    //     }
    //     vm.expirationDate = decodeURI(vm.expirationDate);
    //     console.log(vm.expirationDate);
    // }
    
    if ($location.absUrl().indexOf('token_type')>0)
    {
        vm.token_type = $location.absUrl().split('token_type=')[1];
        if (vm.token_type.indexOf('&')>0)
        {
            vm.token_type = vm.token_type.split('&')[0];
        }
    }

    // console.log(vm.access_token);
    // sharedProperties.setAccessToken(vm.access_token);
    // console.log(sharedProperties.getAccessTokenAuthHeader());

    var oauth2_data_json = {
            access_token: vm.access_token, 
            // expirationDate: vm.expirationDate,
            token_type: vm.token_type
        };

    //Guardamos la información de token en una cookie
    sharedProperties.setTokenData(oauth2_data_json);

    // if ($cookies.getObject('oauth2_data'))
    // {
    //     console.log($cookies.getObject('oauth2_data'));
    //     //console.log(JSON.parse($cookies.getObject('oauth2_data')));
    //     //console.log(JSON.parse($cookies.getObject('oauth2_data')).access_token);
    //     //console.log(JSON.parse($cookies.getObject('oauth2_data')).expirationDate);
    //     //console.log(JSON.parse($cookies.getObject('oauth2_data')).token_type);
    // }

    // $cookies.remove('oauth2_data');
    // $cookies.putObject('oauth2_data', JSON.stringify(oauth2_data_json), {path: '/'});

    window.location = "/";
    
}