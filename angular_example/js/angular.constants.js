angular.module('myConfig',[]) 
	.constant('oauth2_constants',{
 		redirect_uri : 'http://localhost:3000/login/logged/',
 		client_id: 'test1',
 		cookies_expiration_time: 18000000,
 		cookies_name: 'oauth2_data'
	});