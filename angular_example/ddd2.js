angular
        .module('apiApp1', ['ngCookies'])
        .controller('apiAppCtrl1', ['$http','$scope','$cookies','$cookieStore', controladorPrincipal]);
        //.controller('apiAppCtrl', ['$http', controladorPrincipal]);
    function controladorPrincipal($http,$scope,$cookies,$cookieStore){
        var vm=this;
        
        vm.buscaEnRegion = function(){

            var someSessionObj = { 'innerObj' : 'somesessioncookievalue'};

            alert($cookies.dotobject);
            alert($cookies.usingCookies);

            $cookies.dotobject = someSessionObj;
            $scope.usingCookies = { 'cookies.dotobject' : $cookies.dotobject, "cookieStore.get" : $cookieStore.get('dotobject') };

            $cookieStore.put('obj', someSessionObj);
            $scope.usingCookieStore = { "cookieStore.get" : $cookieStore.get('obj'), 'cookies.dotobject' : $cookies.obj, };
        }
    }