var csv = require("fast-csv");
var mongoose = require('mongoose');
var timezone = require('./server/models/timezone.server.model');
var country = require('./server/models/country.server.model');
var location = require('./server/models/location.server.model');
var category = require('./server/models/category.server.model');

mongoose.connect('mongodb://localhost:27017/marbuApp');


// csv
//  .fromPath("TimeZones.txt", {delimiter:'\t', 
//  	headers : [
//  		"ISO_Country",
//  		"name",
//  		"GMT_offset_jan2016",
//  		"GMT_offset_jul2016",
//  		"raw_offset"
//  	]})
//  .on("data", function(data){
//  	console.log("-------------> data");
//  	console.log(data);
//  	var d = new timezone(data);
//  	console.log(d);
//  	d.save(function(err){});

//  })
//  .on("end", function(){
//      //console.log(timezones);
//  });



// var countries = [];
// var i = 0;
// csv
//  .fromPath("countries.txt", {delimiter:'\t', 
//  	headers : [
//  		"ISO",
//  		"ISO3",
//  		"ISO_num",
//  		"fips",
//  		"name",
//  		"capital",
//  		"area",
//  		"population",
//  		"continent",
//  		"TLD",
//  		"CurrencyCode",
//  		"CurrencyName",
//  		"PhonePrefix",
//  		"PostalCodeFormat",
//  		"PostalCodeRegex",
//  		"Languages",
//  		"geonameId",
//  		"neighbours",
//  		"EquivalentFipsCode"
//  	]})
//  .on("data", function(data){
// 	var d = new country(data);
// //  	console.log(d);
// 	d.save(function(err){
// 		console.log(i++);
// 		if (err)
// 		{
// 			console.log(err);
// 		}
// 	});
//  	countries.push(data);

//  })
//  .on("end", function(){
//      console.log("done");
//      console.log(countries.length);
//  });


/*

var regions = [];

csv
 .fromPath("ES.txt", {delimiter:'\t', headers : ["geonameId","name","asciiName","alternatenames","latitude","longitude","feature_class","feature_code","country_code","cc2","admin1_code","admin2_code","admin3_code","admin4_code","population","elevation","dem","timezone","modification_date"]})
 .on("data", function(data){

    var feat_class = data.feature_class;
    var feat_code = data.feature_code;
    if (feat_class === "A")
	{
		if (feat_code === "ADM1")
		{
			regions[data.country_code+"."+data.admin1_code] = data.name;
		}
		else if (feat_code === "ADM2")
		{
			regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code] = data.name;
		}
		else if (feat_code === "ADM3")
		{
			regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code+"."+data.admin3_code] = data.name;
		}
		else if (feat_code === "ADM4")
		{
			regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code+"."+data.admin3_code+"."+data.admin4_code] = data.name;
		}

	}
 })
 .on("end", function(){
    console.log("done");
    console.log(regions);

    var locations = [];
	var i = 0;
	csv
	 .fromPath("ES.txt", {delimiter:'\t', headers : ["geonameId","name","asciiName","alternatenames","latitude","longitude","feature_class","feature_code","country_code","cc2","admin1_code","admin2_code","admin3_code","admin4_code","population","elevation","dem","timezone","modification_date"]})
	 .on("data", function(data){

	 	if (data.feature_class === "A" && (data.feature_code === "ADM1" || data.feature_code === "ADM2" || data.feature_code === "ADM3" || data.feature_code === "ADM4"))
		{
			var loc = {
				type: "region",
				name: data.name,
				country: data.country_code,
				latitude: data.latitude,
				longitude: data.longitude,
				timezone: data.timezone
			};

			if (data.admin1_code && data.admin2_code)
			{
				loc['loc_level1'] = regions[data.country_code+"."+data.admin1_code];
			}

			if (data.admin1_code && data.admin2_code && data.admin3_code)
			{
				loc['loc_level2'] = regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code];
			}

			if (data.admin1_code && data.admin2_code && data.admin3_code && data.admin4_code)
			{
				loc['loc_level3'] = regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code+"."+data.admin3_code];
			}

			var d = new location(loc);
		  	// console.log(d);
			d.save(function(err){
			// console.log(i++);
				if (err)
				{
					console.log(err);
				}
			});
		}
	    else if (data.feature_class === "P")
		{
			var loc = {
				type: "location",
				name: data.name,
				country: data.country_code,
				latitude: data.latitude,
				longitude: data.longitude,
				timezone: data.timezone
			};

			if (data.admin1_code)
			{
				loc['loc_level1'] = regions[data.country_code+"."+data.admin1_code];
			}

			if (data.admin2_code)
			{
				loc['loc_level2'] = regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code];
			}

			if (data.admin3_code)
			{
				loc['loc_level3'] = regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code+"."+data.admin3_code];
			}

			if (data.admin4_code)
			{
				loc['loc_level4'] = regions[data.country_code+"."+data.admin1_code+"."+data.admin2_code+"."+data.admin3_code+"."+data.admin4_code];
			}

			var d = new location(loc);
		  	// console.log(d);
			d.save(function(err){
			// console.log(i++);
				if (err)
				{
					console.log(err);
				}
			});
			locations.push(loc);
		}

	 })
	 .on("end", function(){
	     console.log("done");
	     console.log(locations.length);
	 });

 });
*/


csv
 .fromPath("ex.txt", {delimiter:'\t', 
 	headers : [
 		"name",
 		"description"
 	]})
 .on("data", function(data){
 	console.log("-------------> data");
 	console.log(data.name);
 	console.log(JSON.parse(data.name));
 	console.log(JSON.parse(data.name).es);
 	console.log(JSON.parse(data.name).en);
 	var d = new category(data);
 	console.log(d);
 	d.save(function(err){
 		console.log(err);
 	});

 })
 .on("end", function(){
     //console.log(timezones);
 });

